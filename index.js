require('./styles.css');

import rxjs, { Observable } from 'rxjs';
import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { connect, Provider } from 'react-redux';
import { createEpicMiddleware } from 'redux-observable';

import { getUserActionCreator, getUserCancelActionCreator } from './actionCreators.js';
import { GET_USER_ACTION_TYPE } from './actionTypes.js';
import reducer from './reducer.js';
import { getUser } from './api.js';


const epic = (action$, store) => action$
      .ofType(GET_USER_ACTION_TYPE)
      .mergeMap(function (action) {
          return Observable
              .fromPromise(getUser(action.id))
              .map(response => ({ type: `${action.type}_SUCCESS`, response }))
              .catch(err => ({ type: `${action.type}_FAILURE`, err }))
              .takeUntil(action$.ofType(`${action.type}_CANCEL`));
      });


const epicMiddleware = createEpicMiddleware(epic);
const store = createStore(reducer, applyMiddleware(epicMiddleware));



function SampleComponent(props) {
    const { caption, canCancel } = props;
    const { getUser, cancel } = props;
    return (
        <div className="sampleComponent">
            <div className="userInfo">User info: { caption }</div>
            <button onClick={ () => getUser(0) }>Get user</button>
            <button onClick={ () => cancel() } disabled={ !canCancel } >Cancel</button>
        </div>
    );
}

const SampleComponentRdx = connect(
    function (state, ownProps) {
        let caption = '';
        let canCancel = state.loading;
        if (state.loading) {
            caption = '...loading';
        } else if (state.user) {
            caption = state.user.name;
        }
        return { caption, canCancel };
    },
    (dispatch, ownProps) => ({
        getUser: (id) => dispatch(getUserActionCreator(id)),
        cancel: () => dispatch(getUserCancelActionCreator())
    })
)(SampleComponent);


const container = document.createElement('div');
document.body.appendChild(container);
ReactDOM.render(
    (<Provider store={ store }>
         <div className="wrap"><SampleComponentRdx /></div>
     </Provider>),
    container
);



function thunkStyleActionCreator(id) {
    return (dispatch) => {
        dispatch('GET_USER_PENDING');
        getUser(id)
            .then(dispatch('GET_USER_FULFILLED'))
            .catch(dispatch('GET_USER_REJECTED'));
    };
}


function promiseMiddlewareStyleActionCreator(id) {
    return {
        type: 'GET_USER',
        payload: getUser(id)
    };
}


function fakeEpic(action$, store) {
    return action$
        .do(() => console.log(store.getState()))
        .ofType(GET_USER_ACTION_TYPE)
        .mergeMap(function (action) {
            return Observable.of(
                { type: 'SOME_FAKE_ACTION' },
                { type: `${action.type}_SUCCESS`, response: { name: 'Ann' } }
            )
                .delay(500)
                .takeUntil(action$.ofType(`${action.type}_CANCEL`));
        });
}
