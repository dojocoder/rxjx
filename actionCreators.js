import { GET_USER_ACTION_TYPE } from './actionTypes.js';

const getUserActionCreator = (param) => ({ type: GET_USER_ACTION_TYPE, param });
const getUserCancelActionCreator = () => ({ type: `${GET_USER_ACTION_TYPE}_CANCEL` });

export {
    getUserActionCreator,
    getUserCancelActionCreator
};
