import { createReducer } from './utils.js';
import { GET_USER_ACTION_TYPE } from './actionTypes.js';

const reducer = createReducer({ loading: false }, {
    [GET_USER_ACTION_TYPE](state, action) {
        return {
            ...state,
            loading: true
        };
    },
    [`${GET_USER_ACTION_TYPE}_SUCCESS`](state, action) {
        return {
            ...state,
            loading: false,
            user: action.response
        };
    },
    [`${GET_USER_ACTION_TYPE}_CANCEL`](state, action) {
        return {
            ...state,
            loading: false
        };
    }
});

export default reducer;
