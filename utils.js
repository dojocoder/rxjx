function createReducer(initialState = {}, handlers) {
    return function (state = initialState, action) {
        console.group(`${ action.type } (log comes from reducer)`);
        console.log('state %o', state);
        console.log('action %o', action);
        console.groupEnd();

        if (!handlers.hasOwnProperty(action.type)) {
            return state;
        }
        return handlers[action.type](state, action);
    };
}

export {
    createReducer
};
